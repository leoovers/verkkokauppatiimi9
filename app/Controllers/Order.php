<?php 
namespace App\Controllers;
use App\Models\OrderModel;


class Order extends BaseController{

    public function __construct() {
		$session = \Config\Services::session();
        $session->start();
        
	}

    public function submit() {

        $orderModel= new OrderModel();

        $customer = [
'etunimi' => $this->request->getPost('fname'),
'sukunimi' => $this->request->getPost('lname'),
'lahiosoite' => $this->request->getPost('address'),
'postinumero' => $this->request->getPost('zip'),
'postitoimipaikka' => $this->request->getPost('city'),
'email' => $this->request->getPost('email')

    ];

    $orderModel->saveAll($customer,$_SESSION['cart']);
    unset($_SESSION['cart']);
    echo view('templates/header' );
	echo view('thankyou_view');
	echo view('templates/footer');
 
}

}