<?php 
namespace App\Controllers;
use App\Models\StoreModel;


class Store extends BaseController{
    public function __construct() {
		$session = \Config\Services::session();
		$session->start();
		if (!isset($_SESSION['cart'])) {
			$_SESSION['cart'] = array();
		}
		$this->StoreModel = new StoreModel();
	}
    
    public function index(){
        $model=new StoreModel();
        $data['products']=$model->getProducts();
        $data["title"] = "NoiseOutlet";
        echo view('templates/header', $data);
        echo view('index_view',  $data);
        echo view('templates/footer');
    }

    public function add($id) {
       
		array_push($_SESSION['cart'],$id);
       
		return redirect()->to(site_url('/Store/Product/' . $id));	
	}
    public function headphones(){
        $model=new StoreModel();
        $data['headphones']=$model->getHeadphones();
        $data["title"] = "Headphones - NoiseOutlet";
        echo view('templates/header', $data);
        echo view('headphones_view',$data);
        echo view('templates/footer');

    }
    
    public function speakers(){
        $model=new StoreModel();
        $data['speakers']=$model->getSpeakers();
        $data["title"] = "Speakers - NoiseOutlet";
        echo view('templates/header', $data);
        echo view('speakers_view', $data);
        echo view('templates/footer');
    }


    public function microphones(){
        $model=new StoreModel();
        $data['microphones']=$model->getMicrophones();
        $data["title"] = "Microphones - NoiseOutlet";
        echo view('templates/header', $data);
        echo view('microphones_view', $data);
        echo view('templates/footer');
    }
    public function players(){
        $model=new StoreModel();
        $data['players']=$model->getPlayers();
        $data["title"] = "Players - NoiseOutlet";
        echo view('templates/header', $data);
        echo view('Players_view', $data);
        echo view('templates/footer');
    }
    public function product($id){
        
        $model=new StoreModel();
        
        $data['product']=$model->getProduct($id);
        $data["title"] = "Product - NoiseOutlet";
        echo view('templates/header', $data);
        echo view('product_view', $data);
        echo view('templates/footer');
    }
    
    
    
    
}
