<?php 
namespace App\Controllers;
use App\Models\CartModel;
use App\Models\StoreModel;

class Cart extends BaseController{
    
    public function __construct() {
		$session = \Config\Services::session();
		$session->start();
		if (!isset($_SESSION['cart'])) {
			$_SESSION['cart'] = array();
		}
        $this->CartModel = new CartModel();
        $this->StoreModel = new StoreModel();
	}    
    
    public function cart(){
        if (count($_SESSION['cart']) > 0) {
			$Products = $this->StoreModel->Getforarray($_SESSION['cart']);
		}
		else {
			$Products = array();
		}

    $data['cart'] = $Products;
		$model=new CartModel();
        $data['products']=$model->getProducts();
        $data["total"]=$model->getTotal();
        $data["title"] = "Ostoskori";
        
        echo view('templates/header', $data);
        echo view('cart_view', $data);
        echo view('templates/footer');
    }

    public function order() {

        $model=new CartModel();
        $data["title"]="Tilauslomake";
        echo view('templates/header', $data);
        echo view('order_view', $data);
        echo view('templates/footer');

    }

    public function deleteFromCart($product_id) {
		// Käydään läpi taulukko lopusta päin ja poistetaan kaikki tuotteet.
		// Jos poistamista lähtee tekemään alusta päin ja poistaa toiston sisällä tuotteita, saa
		// index out of bounds virheen. Sama tuote voi olla moneen kertaan istuntomuuttujan taulukossa, 
		// joten koko taulukko pitää käydä läpi.
		for ($i = count($_SESSION['cart'])-1; $i >= 0;$i--) {
			if ($_SESSION['cart'][$i] === $product_id) {
				array_splice($_SESSION['cart'], $i, 1);
				// Jos halutaan, että poistetaan vain yksi kappale,
				// lisätään return lause tähän.
				//return redirect('ostoskori');
			}
		}
		return redirect('cart');
	}

}