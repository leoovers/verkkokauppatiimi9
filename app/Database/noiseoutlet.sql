drop database if EXISTS noiseoutlet;

create database noiseoutlet;

use noiseoutlet;

create table asiakas (
  id int primary key auto_increment,
  etunimi varchar(50) not null,
  sukunimi varchar(100) not null,
  lahiosoite varchar(100),
  postinumero char(5),
  postitoimipaikka varchar(100),
  email varchar(255)
);

create table tuoteryhma
(
    tuoteryhmanro SMALLINT PRIMARY KEY,
    tuoteryhmanimi varchar(100) not null
);

create table tuote (
     id int primary key auto_increment,
      nimi varchar
(100) not NULL UNIQUE,
       kuvaus text not null,
        hinta DECIMAL
(10,2),
         tuoteryhmanro SMALLINT,
          FOREIGN KEY
(tuoteryhmanro) 
          REFERENCES tuoteryhma
(tuoteryhmanro)
           on
delete RESTRICT );




create table tilaus (
  id int primary key auto_increment,
  tila enum ('tilattu','toimitettu','maksettu'),
  tilattu timestamp default current_timestamp,
  asiakas_id int not null,
  index (asiakas_id),
  foreign key (asiakas_id) references asiakas(id)
  on delete restrict
);
create table tilausrivi (
  tilaus_id int not null,
  index (tilaus_id),
  foreign key (tilaus_id) references tilaus(id)
  on delete restrict,
  tuote_id int not null,
  index (tuote_id),
  foreign key (tuote_id) references tuote(id)
  on delete restrict,
  maara smallint
);


INSERT INTO tuoteryhma
VALUES
    (1, 'kuulokkeet');


INSERT INTO tuoteryhma
VALUES
    (2, 'kaiuttimet');

INSERT INTO tuoteryhma
VALUES
    (3, 'mikrofonit');

INSERT INTO tuoteryhma
VALUES
    (4, 'soittimet');

INSERT INTO tuoteryhma
VALUES
    (5, 'tarvikkeet');

INSERT INTO tuote
    (id,nimi,kuvaus,hinta,tuoteryhmanro)
values
    (1, "JBL TUNE700BT MUSTA BLUETOOTH-KUULOKKEET", "Tehokkaan ja muhkean JBL Pure Bass -äänen sävyttämänä JBL TUNE 700BT -kuulokkeet toistavat ääntä upean täyteläisesti ja langattomasti. 
Kevyt korvan päälle asettuva design tarjoaa parhaan mahdollisen käyttömukavuuden ja saa sinut uppoutumaan äänimaailman nautintoihin. 
Nämä suorituskykyiset kuulokkeet taittuvat kompaktiin muotoon ja kulkevat helposti mukana kaikissa käänteissä. 
Voit vastata puheluihin ja hallinnoida ääntä handsfree-tilassa kuulokekuppiin sijoitetuilla helppokäyttöisillä painikkeilla. 
Niiden avulla voit myös aktivoida puheavustajan saadaksesi apua liikkeellä ollessasi. 
Nauti kuulokkeistasi koko päivä 27 tuntia kestävän akun ansiosta tai rajoituksetta mukana tulevalla erillisellä audiokaapelilla. 
Monipisteyhteys yhdistää kaksi Bluetooth® -laitetta samanaikaisesti ja takaa, että huomaat varmasti kaikki puhelimellesi tulevat puhelut
suoratoistaessasi videoita tabletillasi. 

Taajuusalue  20Hz-20kHz
Kuuloke-elementin impedanssi 32 ohmia
Maks. tehonkesto 40mW
Paino (g) 220 g
Kuulokkeiden akkutyyppi  Polymeeri-litium-ioniakku (610mAh/3.7V)
Virtalähde 5V ⎓ 1A
Latausaika <2 tuntia tyhjästä
Musiikin toistoaika BT päällä  27 tuntia

Paketin sisältö:

1 pari JBL TUNE 700BT -kuulokkeita
Latauskaapeli
Irrotettava audiokaapeli
Varoituskortti
Takuukortti
QSG", 69, 1);

INSERT INTO tuote
    (id, nimi, kuvaus, hinta, tuoteryhmanro)
VALUES
    (2, 'BEYERDYNAMIC DT880',
        'Kannettavaan käyttöön sopiva versio suositusta DT 880 kuulokkeesta. 
Matalan 32 ohmin impedanssin ansiosta erillistä kuulokevahvistinta ei välttämättä tarvita. Beyerin klassikko joka on optimoitu mobiililaitteiden käyttöön. 
Impedanssi on 32 ohm joten se on \"helppo ajettava\" jopa mobiililaitteestasi. 
Rakenne on hifistien arvostama puoliavoin joka antaa avoimen ja moniuloitteisen äänikuvan. 
Nämä kuulokkeet ovat valmistettu Saksassa parhaimman laadun takaamiseksi.', 199.00, 1);

INSERT INTO tuote
    (id,nimi,kuvaus,hinta,tuoteryhmanro)
VALUES
    (3, "DALI OBERON 3 JALUSTAKAIUTIN WHITE", "DALI OBERON on läpimurto! Se on Dalin ensimmäinen niin sanottu entry-level kaiutinmallisto, joka sisältää Dalin patentoiman SMC-teknologian elementeissään. SMC-teknologia vähentää radikaalisti epälineaarista magneettista äänen säröytymistä. Näin pystyt kuulemaan mielimusiikkisi tavalla jota et ole ennen kokenut.

Uusilla ylisuurilla diskanteilla, laajan suuntaavuuden puukuitu-woofereilla ja hienostuneella Tanskalaisella muotoilullaan DALI OBERON asettaa kilpailijoitaan vastaan riman korkeammalle huokeampien Hi-Fi-kaiuttimien sarjassa.

On aika löytää uudelleen musiikin taika!", 329.00, 2);

INSERT INTO tuote
    (id,nimi,kuvaus,hinta,tuoteryhmanro)
VALUES
    (4, "KLIPSCH R-51PM AKTIIVIKAIUTINPARI", "Ominaisuuksia:
120W kokonaisteho
Bluetooth® langaton teknologia aptX standardilla
Levysoitin etuvahvistin maaadoitetulla liitännällä
3.5mm / RCA analogiset sisääntulot
USB ja optinen digitaalitulo
Subwoofer ulostulo
Kaukosäädin



Tekniset tiedot:

Taajuus vaste: 68Hz - 21kHz
Maksi äänenpaine 107dB @ 1M
Todellinen teho: 70W Total System Power (140W Peak)
Jakotaajuus 2000 Hz (Passive)
Kotelon materiaali: MDF
Kotelon tyyppi: Basso-refleksi
Sisääntulot: Bluetooth®, Phono/Line analog, 3.5mm analog mini jack, USB digital, Optical digital
Ulostulot: RCA-linja subwooferille.
Korkeus, leveys, Syvyys (24.8cm),  (14.9cm),   (19.1cm)
Paino  10,6kg
Valmistettu vuodesta 2018", 599, 2);

INSERT INTO tuote
    (id, nimi, kuvaus, hinta, tuoteryhmanro)
VALUES
    (5, "Sangeon euphonic 450 (bluetooth speaker)", "Euphonic 450 (WR-45) on tyylikäs FM-radio, joka tekee päivästäsi paremman korkealaatuisella äänenlaadulla ja erinomaisella vastaanotolla. Radiota voi käyttää sisäisellä antennilla tai lanka antennilla. Radion lisäksi voit kuunnella lempimusiikkia Bluetoothin kautta tai liittää äänilähteesi lisälaite sisääntuloon.
 
Bassorefleksi putki korostaa Euphonicin 450 ainutlaatuista muotoilua. Puinen kotelo vahvistaa erinomaista äänenlaatua. Tämä houkutteleva radio hämmästyttää sinua ja kaikkia, jotka sen näkevät. Lataaminen onnistuu mukana toimitetulla Micro USB -kaapelilla ja sinulla on yli 25 tuntia radiota.", 179.00, 2);

INSERT INTO tuote
    (id, nimi, kuvaus, hinta, tuoteryhmanro)
VALUES
    (6, "KEF Blade", "KEF Blade referenssikaiutinpari.

Voiko kaiuttimen ääni salvata hengityksen? Kyllä.

Silkka Blade-kaiuttimen nopeus saa haukkomaan happea. Tämä siis sen jälkeen, kun on toipunut kaiuttimen ensinäkemisen aiheuttamasta huimauksesta.

Maailman ensimmäinen single apparent source -kaiutin ravistelee täydelliseksi luultuja oppeja huippuluokan kaiutinsuunnittelun saralla. Blade esiteltiin ensimmäisen kerran jo muutama vuosi sitten.

Sen teknologia oli vielä tuolloin liian kallis sarjatuotantoon erityisesti vaativan kotelorakenteen johdosta. Nyt Blade on kuitenkin realiteetti parasta havittelevalle audiofiilille.

4 kpl 9 tuuman vastakkain iskevää bassoelementtiä muodostavat soidessaan jättimäisen yhtälön kotelon etuosassa sijaitsevan, viimeisimmän kehitysversion, Uni-Q elementin kanssa.

Blade on kaiutin, jonka suorituskykyä eivät sanat riitä kuvaamaan.

", 26900.00, 2);

INSERT INTO tuote
    (id, nimi, kuvaus, hinta, tuoteryhmanro)
VALUES
    (7, "Apple AirPods Pro", "AirPods Pro -kuulokkeet on suunniteltu tuottamaan vangitseva äänielämys aktiivisen taustamelun vaimennuksen avulla. Läpikuulumistilan ansiosta voit kuulla ympäristön äänet. Kuulokkeiden istuvuus on säädettävissä, joten niitä käyttää mukavasti vaikka koko päivän. AirPodien tavoin AirPods Pro -kuulokkeet muodostavat taianomaisesti yhteyden iPhoneesi tai Apple Watchiisi. Ja ne ovat käyttövalmiit suoraan kotelosta.

Uskomattoman kevyet AirPods Pro ‑vastamelukuulokkeet sulkevat ympäristön äänet pois, joten voit keskittyä siihen mitä kuuntelet. Aktiivinen taustamelun vaimennus mukautuu jatkuvasti korvasi geometriaan ja korvatulppien istuvuuteen. Se vaimentaa ympäröivän maailman, jotta voit keskittyä täysillä musiikkiin, podcasteihin ja puheluihin.

Kun vaihdat läpikuulumistilaan, AirPods Pro -kuulokkeet päästävät taas taustaäänet läpi ja voit hahmottaa ympäristösi. AirPods Pro ‑kuulokkeiden ulos- ja sisäänpäin suunnatut mikrofonit pystyvät torjumaan silikonitulppien ääntä eristävän vaikutuksen, jolloin kaikki kuulostaa luonnolliselta kuin keskustellessasi normaalisti muiden kanssa.

Sisältä ohennetut tulpat tarjoavat säädettävän istuvuuden. Valittavanasi on kolme kokoa. Ne mukautuvat korvasi muotoon pitäen AirPods Pro ‑kuulokkeet pysyvästi paikoillaan. Tiivis istuvuus takaa erinomaisen taustamelun vaimennuksen.

Erikoisvalmisteinen, vääristämätön high-excursion-ääniajuri tuottaa tuhdin basson. Huipputehokas laajan äänialan vahvistin tuottaa erittäin puhtaan äänen ja pidentää samalla akun käyttöaikaa. Adaptiivinen taajuuskorjaus säätää musiikin automaattisesti korvasi muotoon sopivaksi, jolloin kuuntelukokemus on sävykäs ja yhtenäinen.

Applen suunnittelema H1-siru tuottaa uskomattoman pienen ääniviiveen. Voit helposti hallita musiikkia ja puheluita varressa olevalla painalluksen tunnistimella tai vaihtaa taustamelun vaimennuksen ja läpikuulumistilan välillä. Viestien ilmoitus Sirillä tarjoaa mahdollisuuden luettaa tekstisi Sirillä AirPodien kautta. Äänijaon ansiosta sinä ja ystäväsi voitte striimata ääntä yhtä aikaa kahdella AirPods-kuulokeparilla. Voitte siis pelata, katsella elokuvaa tai kuunnella musiikkia yhdessä.

Jopa 4,5 tuntia kuunteluaikaa yhdellä latauksella. Jopa 5 tuntia aktiivisen taustamelun vaimennuksen ja läpikuuluvuuden ollessa pois päältä. Jopa 3,5 tuntia puheaikaa yhdellä latauksella. Latauskotelo Yli 24 tuntia kuunteluaikaa, Yli 18 tuntia puheaikaa. 5 minuuttia kotelossa antaa noin 1 tunnin kuunteluaikaa.

Pakkauksen sisältö

AirPods Pro -nappikuulokkeet
Langaton latauskotelo
Silikonitulpat (kolme kokoa)
Lightning–USB-C-kaapeli
AirPods Pro ‑kuulokkeet ja ympäristö.

Apple huomioi tuotteen koko elinkaaren määrittäessään ympäristö­kuormitustaan.
AirPods Pro -kuulokkeissa on seuraavat ympäristö­kuormitusta vähentävät ominaisuudet:

Ei bromattuja palonesto­aineita
Ei PVC:tä
Ei berylliumia

", 250.00, 1);

INSERT INTO tuote
    (id,nimi,kuvaus,hinta,tuoteryhmanro)
values
    (8, "MADBOY U-TUBE 10 LANGATON MIKROFONI", "U-TUBE 10 on taattua MadBoy laatua. Mikrofonissa on käytetty muista MadBoy mikrofoneista tuttua käsilähetinmallia ja kapselia. Se tarkoittaa sitä, että soundin puolesta sinun ei siis tarvitse jännittää...

Laulajat toivoivat laadukasta mutta edullista langatonta yhden mikrofonin settiä. MadBoy toteutti toiveen.", 99, 3);

INSERT INTO tuote
    (id,nimi,kuvaus,hinta,tuoteryhmanro)
values
    (9, "MADBOY U-REMIX BLUETOOTH MIKSERI&LANGATTOMAT MIKROFONIT", "MadBoy U-REMIX yhdistää älykkäällä tavalla langattomat mikrofonit, USB/SD soittimen ja Bluetooth mikserin. Langattomien mikrofonien liittäminen erilaisiin äänentoistoihin on entistä helpompaa.

U-REMIX ratkaisee ongelmasi kun tarvitset karaokelaitteet tai mikrofonit esimerkiksi autoosi. Asennuskohteita voivat olla asuntoautot, bussit, karaoketaksit, jne... Voit ottaa virran suoraan auton tupakansytytinliitännästä.

Tokihan laite soveltuu myöskin tavalliseen äänentoistokäyttöön, jolloin voit liittää siihen esim tietokoneesi, soittimesi, kotistereosi, tietokonekaiuttimesi tai lähes mitä vain AV-tekniikkaa.

Laitteessa on Bluetooth yhteys, jolla voit toistaa musiikkia langattomasti. Tämä on mukava lisä kun äänilähteenäsi on esim. kannettava tietokone, tabletti ta matkapuhelin.", 199, 3);

INSERT INTO tuote
    (id,nimi,kuvaus,hinta,tuoteryhmanro)
values
    (10, "MADBOY U-TUBE 20P LANGATON 32 KANAVAINEN MIKROFONISETTI", "Häiritseekö langattomista mikrofoneistasi kuuluva vieras laulanta omaa karaokeiltaasi? 
Kas vain, naapurisi on myöskin sattunut ostamaan juuri samalla taajuudella toimivat langattomat mikrofonit.
Tätä sattuu... 

Tai onko järjestettävänäsi tilaisuus, jossa tarvitaan monta yhtäaikaista langatonta mikrofonia, mutta nykyinen järjestelmäsi ei salli useiden mikrofonien yhtäaikaista käyttöä?
Tätäkin sattuu...

Tällaiset ongelmat ovat historiaa kun käytettävänäsi on MadBoy U-TUBE 20P.
MadBoy U-TUBE 20P on kahden langattoman mikrofonin setti, jossa on yhteensä 32 vaihdettavaa kanavaa.
Jos siis käyttämälläsi kanavalla esiintyy häiriötä, vaihdat vain toiselle kanavalle ja homma toimii taas.
U-TUBE 20P toimii uudella yleiseurooppalaisella 823.000 MHz - 832.000 MHz mikrofonitaajuusalueella. 

Käytettäessä mikrofonit tuntuvat erittäin mukavilta kädessä. Soundi on miellyttävän pehmeä ja selvä.
U-TUBE 20P käyttää samaa kapselia kuin U-TUBE 20R, joten setti soveltuu loistavasti myös ammattikäyttöön ja kilpailee laadulla huomattavasti kalliimpien brändien kanssa.
Mikrofonin kätevä LCD näyttö ilmoittaa käytettävän kanavan, taajuuden ja pariston tilan.
Sinun ei siis tarvitse enää arvuutella paljonko paristoissa on virtaa jäljellä. Tarkista se näytöltä. 

Täysmetallinen vastaanotin on tehty kestämään ammattikäytön haasteet.
Mikrofonien äänenvoimakkuudensäätimet ja kanavavalitsimet on kätevästi etupaneelissa.
Selkeä LCD näyttö näyttää mikrofonien kanavanumeron, taajuuden ja signaalivahvuuden.
Antenniliittimet, balansoidut XLR-ulostulot molemmille kanaville, sekä balansoimaton 6,3 plugi ulostulo sijaitsevat vastaanottimen takapaneelissa.

U-TUBE 20P on järkevä valinta kun tarvitset laatua, häiriötöntä toimintavarmuutta tai useita yhtäaikaisia langattomia mikkejä.", 335, 3);


INSERT INTO tuote (id, nimi, kuvaus, hinta, tuoteryhmanro)
VALUES (12, "Sennheiser RS 195 ", "Suljetut huippulaadukkaat langattomat kuulokkeet kotikäyttöön. Lähettimessä on sekä optinen että analoginen sisääntulo, joten kuulokkeet ovat yhdistettävissä mahdollisimman moneen laitteeseen parhaalla mahdollisella laadulla. Erikseen kytkettävä Hearing-boost -asetus korostaa ja selkeyttää puheäänen taajuusaluetta. Music-käyttötila toistaa äänen musiikin rehellisesti ja tarkasti juuri sellaisena kuin se on tarkoitettu.

Tekniset tiedot:

    Optinen ja analoginen sisääntulo lataustelakassa
    Taajuusvaste: 17 - 22 000 Hz
    Modulaatio: 8-FSK Digital
    SPL: 117 dB
    Kantama: jopa 100 m (vapaassa kentässä)
    Taajuus: 2.4 - 2.48 GHz
    Käyttöaika: jopa 18 h
    Paino: 340 g
    Monikäyttöinen lähetin toimii myös kuulokkeiden säilytys- ja lataustelakkana
    Mukana HDR 195 -kuulokkeet, TR 195 -lähetinasema, optinen Toslink-kaapeli, analoginen audiokaapeli, virtalähde ja kaksi NiMH AAA-akkua.

", 379, 1);

INSERT INTO tuote (id, nimi, kuvaus, hinta, tuoteryhmanro)
VALUES (11, "Sennheiser Momentum True Wireless 2 ", "Toisen sukupolven Sennheiser Momentum True Wireless 2 -kuulokkeet entistä paremmilla ominaisuuksilla, erinomaisella ergonomialla ja mahtavalla 28 tunnin akunkestolla.

Ensiluokkaiset Bluetooth-kuulokkeet tarjoavat vaativalle kuuntelijalle tinkimättömän äänenlaadun ja ovat erittäin ergonomiset käyttää. Aktiivisella kahden mikrofonin melunvaimennuksella varustetut, täysin langattomat Momentum True Wireless 2 -kuulokkeet tarjoavat jopa 7 tunnin akkukeston, ja virtaviivainen latauskotelo antaa lisävirtaa jopa huimat 21 tuntia. Kuulokkeissa on vakaa, langaton Bluetooth 5.1 -yhteys AAC- ja aptX-tuella. Intuitiivisiin ominaisuuksiin kuuluu muun muassa Transparent Hearing -toiminto, joka mahdollistaa paremman ympäristön huomiointikyvyn sekä Smart Pause, joka keskeyttää toiston automaattisesti, kun jompi kumpi kuuloke otetaan pois korvasta. Höyhenenkevyet kuulokenapit ovat hien- ja roiskeveden kestävät.

Tekniset tiedot:

    Elementti: 7 mm dynaaminen
    Sisäänrakennettu ekvalisaattori
    Aktiivinen melunvaimennus (ANC)
    Transparent Hearing
    Kustomoitavat hipaisuohjaimet
    Smart Pause
    Tuki puheohjaukselle: Google Assistant, Apple Siri
    Bluetooth v5.1 A2DP, AVRCP, HSP, HFP
    Tuetut kodekit: SBC, AAC, aptX
    Akkukesto: 7 tuntia + 21 tuntia latauskotelolla
    Latausaika: noin 1,5 tuntia
    Taajuusvaste: 5 - 21 000 Hz
    Herkkyys: 107 dB SPL (1 kHz / 1 mW)
    THD: <0,08% (1 kHz / 94 dB)
    IPX4, roiskevesisuojattu
    Sovellustuki: Sennheiser Smart Control (iOS ja Android)
    Paino: 6 grammaa /kuuloke, 58 grammaa /latauskotelo
    Pakkauksessa: Momentum True Wireless 2 -kuulokkeet, latauskotelo, USB-C-latauskaapeli, silikonisovitteet (koossa XS/S/M/L), manuaalit
", 299, 1);

INSERT INTO tuote (id, nimi, kuvaus, hinta, tuoteryhmanro)
VALUES (13, "JBL Xtreme 2 -Bluetooth-matkakaiutin ", "JBL Xtreme 2 -Bluetooth-kaiutin tarjoaa vaivattomasti dynaamisen ja mukaansatempaavan stereoäänen. Kaiutin on varustettu neljällä elementillä, kahdella JBL-bassosäteilijällä sekä ladattavalla 10 000 mAh:n litiumioniakulla. Suuren akun ansiosta voit nauttia musiikista jopa 15 tunnin ajan yhtäjaksoisesti. Kaiuttimessa on myös USB-latausportti ja kaiutin on kokonaisuudessaan IPX-7-luokiteltu, eli sen rakenne on vedenpitävä. JBL Connect+ -teknologian myötä voit linkittää jopa 100 JBL Connect+ -ominaisuudella varustettua kaiutinta yhdeksi äänentoistojärjestelmäksi. Kaiuttimessa on integroidut koukut, kestävä metallipohja ja sen kantohihnaan on kiinnitetty ylimääräinen pullonavaaja. Nämä ominaisuudet yhdessä tekevät Xtreme 2 -kaiuttimesta erinomaisen valinnan, olitpa sitten kotona tai puistossa!
JBL Xtreme 2 -Bluetooth-kaiutin tarjoaa vaivattomasti dynaamisen ja mukaansatempaavan stereoäänen. Kaiutin on varustettu neljällä elementillä, kahdella JBL-bassosäteilijällä sekä ladattavalla 10 000 mAh:n litiumioniakulla. Suuren akun ansiosta voit nauttia musiikista jopa 15 tunnin ajan yhtäjaksoisesti. Kaiuttimessa on myös USB-latausportti ja kaiutin on kokonaisuudessaan IPX-7-luokiteltu, eli sen rakenne on vedenpitävä. JBL Connect+ -teknologian myötä voit linkittää jopa 100 JBL Connect+ -ominaisuudella varustettua kaiutinta yhdeksi äänentoistojärjestelmäksi. Kaiuttimessa on integroidut koukut, kestävä metallipohja ja sen kantohihnaan on kiinnitetty ylimääräinen pullonavaaja. Nämä ominaisuudet yhdessä tekevät Xtreme 2 -kaiuttimesta erinomaisen valinnan, olitpa sitten kotona tai puistossa!

Ominaisuudet:

    Bluetooth v.4.2, A2DP, AVRCP, HFP, HSP
    Elementit: 2 x 2,75 inch (basso), 2 x 20 mm (diskantti)
    Nimellisteho: 2 x 20 W RMS
    Taajuusvaste: 55 - 20 000 Hz
    Latausaika: noin 3,5 tuntia
    Akunkesto: jopa 15 tuntia
    IPX-7-luokitus
    USB-latausportti
    Mitat: 136 x 288 x 132 mm
    Paino: 2393 g
", 249, 2);

INSERT INTO tuote (id, nimi, kuvaus, hinta, tuoteryhmanro)
VALUES (14, "Logitech G560 LIGHTSYNC -pelikaiuttimet", "Logitech G560 LIGHTSYNC -PC-pelikaiuttimet takaavat ennenkokemattoman tunnelman. Tämä 2.1-kaiutinjärjestelmä reagoi pelitapahtumiin ja musiikkiin. Mukauta tehosteita lähes 16,8 miljoonalla värillä sekä etu- ja takaosan neljällä valovyöhykkeellä. 240 watin huipputeho (120 watin RMS-teho) saa räjähtävän tehokkaan, alasampuvan bassokaiuttimen ja laajakulmaelementeillä varustetut satelliittikaiuttimet toistamaan äänen kirkkaasti ja puhtaasti. DTS:X Ultra -moduuli antaa tietokonepeleille ja musiikille todentuntua lisäävän 3D-tilaäänen. G560 tarjoaa useita liitäntävaihtoehtoja: USB, 3.5 mm ja Bluetooth. Logitech Easy-Switch -tekniikan avulla voit vaihdella saumattomasti jopa neljän liitetyn laitteen välillä.

Logitech G560 LIGHTSYNC ominaisuudet:

    Kokonaisteho (huipputeho): 240 W
    Kokonaisteho (RMS): 120 W
    Satelliitti: 2 × 30 W
    Bassokaiutin: 1 × 60 W
    DTS: X Ultra -tilaääni
    Bluetooth-versio 4.1
    Bluetooth-yhteydet: 2
    Luotettava 25 metrin toimintasäde, kun kaiuttimiin on näköyhteys
    USB-tulo: 1
    Kuulokeliitäntä: 1
    Yhteensopivuus: PC-tietokoneet USB-yhteyden tai 3,5 mm:n liitännän kautta tai Bluetooth-yhteensopivat laitteet, kuten tietokoneet, älypuhelimet ja taulutietokoneet
    Yhteensopivat käyttöjärjestelmät: Windows 10, Windows 8,1, Windows 7 ja Mac OS X (DTS:X ei tuettu)
    Satelliittikaiuttimien mitat (L x S x K): 166 x 118 x 148 mm
    Satelliittikaiuttimien paino (yhteensä): 1,8 kg
    Bassokaiuttimen mitat (L x S x K): 255 x 207 x 404 mm
    Bassokaiuttimen paino: 5,4 kg
    Myyntipakkauksen sisältö: Kaksi satelliittikaiutinta, bassokaiutin ja virtajohto, USB-johto, käyttäjän dokumentaatio 
", 239, 2);

INSERT INTO tuote (id, nimi, kuvaus, hinta, tuoteryhmanro)
VALUES (15, "Shure SM58SE - dynaaminen mikrofoni", "Shure SM58SE -dynaaminen mikrofoni on/off-katkaisimella on suunniteltu ammattilaisten käyttöön aina studiotilanteista keikoille. Erinomainen mikrofoni laulu- ja puhetilanteisiin. Mikrofoni sisältää sisäisen filtterin, joka vähentää erinomaisesti häiritseviä hengitys- ja 'pop'-ääniä. Suuntakuvionsa ansiosta mikrofoni poistaa tehokkaasti sivuilta tulevat ylimääräiset äänet ja poimii vain laulajan tai puhujan äänen tehokkaasti.

Yleiset ominaisuudet:

On/Off-katkaisin
Suuntakuvio: hertta
Taajuusvaste: 50 - 15 000 Hz
Herkkyys: -54,5 dBV/Pa
Impedanssi: 150 ohm
Säilytyspussi
Paino: 298 g
Mitat: 162 x 51 mm
", 105, 3);

INSERT INTO tuote (id, nimi, kuvaus, hinta, tuoteryhmanro)
VALUES (16, "Jamo JR-4 -ulkokaiutin", "Jamo JR-4 -ulkokaiutin mukautuu puutarhan ympäristöön saumattomasti muistuttamalla kiveä. Luonnollinen ulkonäkö helpottaa kaiuttimen paikan löytämistä ulkotiloissa ja voit asentaa sen jopa paikkoihin, jossa se saattaa yllättää kuulijansa! Kekseliään ulkonäön lisäksi on myös olennaista, että kaiutin kuulostaa hyvältä. Tässä JR-4 onnistuu erinomaisesti neljän tuuman kaiutinelementtinsä ansiosta. Kaiutin toistaa musiikkia yksityiskohtaisesti, dynaamisesti sekä tasapainoisesti. Erinomainen tapa tuoda eloa puutarhatöihin ja myöhemmin ulkojuhliin!

Ominaisuudet:

    Impedanssi: 8 Ohmia / 70 V
    Taajuusvaste: 115 - 20 000 Hz
    Tehonkesto: 50 W
    Elementti: 4  alumiinielementti
    IP-45 -luokitus
    Mitat: 192 x 226 x 176 mm
    Paino: 2,27 kg ", 249, 2);


INSERT INTO tuote (id, nimi, kuvaus, hinta, tuoteryhmanro)
VALUES (17, "SVS SB16-Ultra -subwoofer, Black Oak", "SVS SB16-Ultra -subwoofer pitää sisällään massiivisen 16 tuumaisen elementin ja 1500 wattia tehoa! Tätä tuhtia kokoonpanoa vauhdittaa tehokas Sledge-vahvistin, joka puristaa tehosta viimeisenkin pisaran irti. Monipuolisten säätimien ja DSP-älypuhelin ohjelman avulla voit säätää subwooferin asetukset juuri itsellesi sopivaksi.

Ominaisuudet:

    Elementti: 16
    65 mm Xmax
    8 puhekela
    Sledge STA-1500D -vahvistin
    Teho: 1500 W RMS, 5000 W peak
    Taajuusvaste: 16 - 460 Hz (+-3 dB)
    RCA-sisääntulo ja XLR-sisääntulo
    RCA-lähtö
    Mitat: 510 x 500 x 580 mm
    Paino: 55,3 kg ", 2799, 2);


INSERT INTO tuote (id, nimi, kuvaus, hinta, tuoteryhmanro)
VALUES (18, "Teac TN-180BT -vinyylisoitin", "Teac TN-180BT on analoginen, 3-nopeuksinen levysoitin Phono EQ:lla ja Bluetooth-yhteydellä. Se varustaa nostalgisen vinyylilevyn modernilla Bluetooth-lähetyksellä, jonka myötä kuuntelet suosikkilevyjäsi langattomasti Bluetooth-kaiuttimesta tai -kuulokkeista. Yhdistät levysoittimeen vahvistimen, aktiivikaiuttimet tai Bluetooth-laitteen kytkettävän phono-, line- tai Bluetooth-ulostulon myötä. Hihnavetoinen moottori varmistaa selkeän äänen. Tukeva MDF-runko minimoi tärinää ja sitä myötä epätoivottuja ääniefektejä, tarjoten kirkkaamman ja selkeämmän soundin. Kolme nopeutta ja äänivarren auto-return-mekanismi.

Ominaisuudet:

    Hihnavetoinen puoliautomaattinen levysoitin
    Sisäänrakennettu RIAA-korjain (MM)
    Bluetooth-lähetys BT-kaiuttimiin ja -kuulokkeisiin
    Bluetooth-kantama: noin 10 metriä
    Phono- ja linjaulostulo
    3 nopeutta (33/45/78 RPM)
    Signaali-kohinasuhde: >64 dB (a-weighted, 20 kHz, LPF)
    Mitat: 420 x 105 x 356 mm
    Paino: 4,9 kg
    Mukana pölykansi, kuminen alusta, MM-rasia, EP-adapteri, 1 x RCA-kaapeli, virtalähde, käyttöohje ", 179, 4);

INSERT INTO tuote (id, nimi, kuvaus, hinta, tuoteryhmanro)
VALUES (19, "KEF Q350", "Kef Q350 -jalustakaiutinpari on osa Kefin Q-sarjan kahdeksatta sukupolvea. Alunperin vuonna 1991 julkaistu Q-sarja on voittanut palkintoja julkaisustaan saakka! Ajattoman tyylikkäästi suunnitellut kaiutimmet sisältävät Kefin kuuluisat Uni-Q elementit, joissa diskanttielementti on sijoitettu keskiääni-, sekä bassoelementin keskiosaan. Innovatiivisen suunnittelutyön tuloksena on yksityiskohtaisempi, tarkka kolmiulotteinen äänikuva, joka täyttää tilan jokaisen nurkan!

Ominaisuudet:

    Tyyppi: 2-tie bassorefleksi
    Elementit: 1 x 6,5 alumiini Uni-Q, 1 x 1 alumiini dome HF
    Taajuusvaste: 63 - 28 000 Hz
    Jakotaajuus: 2,5 kHz
    Maksimi ulostulo: 110 dB
    Vahvistinsuositus: 15 - 120 W
    Herkkyys: 87 dB (2,83 V / 1 m)
    Nominaali impedanssi: 8 ohmia (min 3,5 ohmia)
    Mitat: 358 x 210 x 306 mm
    Paino: 7,6 kg", 479, 2);

INSERT INTO tuote (id, nimi, kuvaus, hinta, tuoteryhmanro)
VALUES (20, "Yamaha NX-N500 -MusicCast-kaiutinpari, musta", "TYamaha NX-N500 -aktiivikaiuttimet ovat monipuoliset ja kompaktit HiFi-kaiuttimet, jotka sisältävät vahvistimen langattomilla ominaisuuksilla. Bluetooth- ja WiFi-liitäntämahdollisuuden ansiosta voit liittää älylaitteesi ja televisiosi helposti kaiuttimiin ja nauttia huikeasta äänentoistosta hetkessä. Laadukkaan USB DACin avulla saavutat korkearesoluutioisen PCM 384 kHz/32-bit toiston ja musiikkisi kuulostaa aina hyvältä. Huikeiden ominaisuuksien lisäksi kaiuttimet ovat muotoiltu tyylikkäästi ja näin ollen sopivat sisustukseen kuin sisustukseen mainiosti!
MusicCast takaa laajat langattomat toisto-ominaisuudet aina älypuhelimista tietokoneisiin ja NAS-asemiin. Laajojen langattomien ominaisuuksien lisäksi MusicCast toimii myös monihuonejärjestelmänä muille MusicCast-laitteille. Monihuonetoisto toimii jopa Bluetoothin kautta toistetulla musiikilla ja laitteeseen voi liittää täysin erillisen Bluetooth-kaiuttimen.


Ominaisuudet:

    Taajuusvaste: 54 - 40 000 Hz
    Jakotaajuus: 2 kHz
    Wi-Fi
    MusicCast
    DLNA 1.5
    Airplay
    Bluetooth 2.1 + EDR/A2DP, SBC/AAC
    Sisääntulot: 1 x Optinen digitaalinen, 1x 3,5 mm analoginen, USB
    Ethernet
    Teho: 45 + 45 W
    Mitat: 170 x 285 x 222 mm (oikea), 170 x 285 x 238,6 mm (vasen)
    Paino: 5,7 kg (oikea), 6,2 kg (vasen)

Mukana virtakaapelit molemmille kaiuttimille, sekä balansoitu XLR-kaapeli ja RJ45-kaapeli kaiuttimien välille. Välikaapelien pituus 3 m.",  479, 2);


INSERT INTO tuote (id, nimi, kuvaus, hinta, tuoteryhmanro)
VALUES (21, "Creative Sound BlasterX Katana -kaiutinjärjestelmä", "Japanilaiset Katana-miekat ovat tekniseltä rakenteeltaan täydellisiä ja niiden valmistus on erittäin vaativaa Sound BlasterX Katana kantaa samaa nimeä sulavalinjaisen ja hienostuneen ulkomuotonsa ja uskomattomien äänentoisto-ominaisuuksiensa ja rakenteensa ansiosta. Katana painiikin omassa sarjassaan ja sopii näyttösi alle niin sujuvasti, että se haastaa kaikki muut soundbar-nimellä kulkevat pelikaiuttimet. Sen voimanlähteenä on palkittu usean ytimen DSP-äänenkäsittely, joka tekee Katanasta maailman ensimmäisen aidon UMAS (Under Monitor Audio System) -kaiuttimen.

Katana tarjoaa mukaansatempaavan äänikokemuksen peleihin ja elokuviin BlasterX Acoustic Engine -ohjelman ja Dolby Digital 5.1 -koodauksen purkutoiminnon ansiosta. Tässä kokoonpanossa yhdistyvät täydellisesti tehokkain 24-bittinen korkearesoluutioinen DAC ja kehittynyt viiden elementin kaiutinjärjestelmä. Kolminkertainen vahvistus, jossa kaiuttimen viittä elementtiä ohjataan erikseen DSP-vahvistimilla, tarkoittaa, että Katana tuottaa erittäin tarkan äänen kaikilla äänialueilla. Runsaiden liitäntävaihtoehtojensa ansiosta Katana on taatusti yksi monipuolisimmista ja tehokkaimmista saatavilla olevista kaiuttimista.

Ominaisuudet:

    Mahtuu sujuvasti niin näyttöjen kuin televisionkin alle.
    Sopii kaikkiin kokoonpanoihin, erityisesti usean näytön PC- ja/tai konsolipelijärjestelmiin.
    Kaiuttimen kotelointi on kauniisti muotoiltu ja vahvistettu tukevalla harjatulla alumiinipaneelilla.
    Aurora Reactive -valaistusjärjestelmä - sen alareunassa on laidasta laitaan 49 ohjelmoitavaa LED-valoa.
    Katanassa on viisi elementtiä - kaksi ylöspäin suunnattua keskibassoelementtiä ja kaksi pitkäliikerataista diskanttia soundbarissa sekä yksi pitkäliikkeinen elementti subwooferissa.
    2,5 tuuman (63,5 mm) ylöspäin suunnatut keskibassoelementit
    1,3 tuuman (34 mm) pitkäliikerataiset diskanttielementit
    Pitkäliikerataisessa subwooferissa on erittäin jäykkä paperikartio ja jäykkä matalahävikkinen seinämämateriaali, joka tuottaa puhtaan, yksityiskohtaisen ja koko kehossa tuntuvan basson.
    Katana käyttää täysin uudenlaista kolmoisvahvistettua mallia, jonka kokonaisteho on 75 RMS / huipputeho 150 W ja joka tuottaa koko huoneen täyttävän, tinkimättömän kirkkaan, tarkan ja tasapainoisen äänen ja syvän basson.
    SB-Axx1 on sertifioitu Dolby Digital -purkulaite, joka muuntaa analogisen äänen (stereo tai 5.1-kanavainen) optisesta lähteestä digitaalisiin 5.1-kanavaisiin kokoonpanoihin 24-bittisenä ja 96 kHz:n taajuudella.
    Mukaansatempaavat 3D-peliäänet tietokoneella
    Optisen tulon kautta liitettynä Dolby Digital -sertifioitu Katana tuottaa realistisen digitaalisen 5.1-kanavaisen äänen mahtaviin konsolipeli- ja elokuvailtoihin.
    USB-ÄÄNI: Virtuaalinen 7.1-kanavainen surround-ääni PC-pelaamiseen
    Voit toistaa musiikkia suoraan Katanasta ja nauttia upeasta kuuntelukokemuksesta helposti liittämällä Katanaan lempikappaleesi sisältävän USB-muistitikun ja painamalla toistopainiketta.
    Katana tukee Bluetooth v4.2 -versiota, jonka yhteysetäisyys on jopa 10 metriä, joten voit nauttia kätevästi upeasta langattomasta äänentoistosta.
    Fyysiset mitat (K x L x S) Soundbar: 60.0 x 600.0 x 79.0 mm, Subwoofer: 333 x 130 x 299 mm
    Bluetooth-profiili A2DP (langaton Bluetooth-stereoääni)
    Tuetut koodekit: AAC, SBC
    Liitännät:Bluetooth, Aux-tulo, Optinen tulo, USB-muistitikku, USB-ÄÄNI, Mic-in, Headset out
    Pakkauksen sisältö: Yksi Sound BlasterX Katana -soundbar-kaiutin, yksi Sound BlasterX Katana -subwoofer-kaiutin, yksi verkkolaite, yksi USB-kaapeli, yksi infrapunakaukosäädin (paristo sisältyy), 2 x Wall Mount Brackets, pikaopas", 299, 2);

INSERT INTO tuote (id, nimi, kuvaus, hinta, tuoteryhmanro)
VALUES (23, "Logitech Z906 -5.1-kaiutinjärjestelmä", "Logitech Speaker System Z906 on kotiteatterikaiuttimien lippulaiva, joka toistaa 5.1 monikanavaiset tilaäänet järisyttävällä 500 watin (RMS) kokonaisteholla. Kaiuttimet ovat myös THX-laatusertifioidut, jonka ansiosta kuuntelija voi olla varma leffateatteritason äänentoistokokemuksesta olohuoneessaan. Logitech Z906 toistaa yksityiskohtaiset Dolby Digital- tai DTS-ääniraidat digitaalisen dekooderinsa avulla ja se myös muuntaa kaksikanavaiset stereoäänet kuulijan ympäröiviksi tilaääniksi. Olohuonetta järisyttävät bassoäänet toistetaan 165 watin bassovahvistimella. Logitech Z906 on suunniteltu myös monipuoliseksi, jotta se sopisi mahdollisimman moneen olohuoneeseen. Kotiteatterikaiuttimiin voi liittää jopa kuusi audiolaitetta yhtä aikaa. Näitä voivat olla esimerkiksi televisio, dvd-soitin, digiboksi, Blu-ray-soitin, pelikonsoli tai tietokone. Sisääntuloina näissä voidaan käyttää mm. optisia, RCA- ja 3,5 millimetrin liitäntöjä. Selkeälukuinen näyttö ja pinottava muotoilu tekevät ohjainyksiköstä tyylikkään lisän kodin viihdejärjestelmiin. Langattomalla kaukosäätimellä voi hallita kuuntelukokemusta suoraan sohvalta ja seinään kiinnitettävät satelliittikaiuttimet muuttavat olohuoneen kotiteatteriksi.

Ominaisuudet:

    Kaiuttimien teho: 5 x 67 W
    Subwooferin teho: 165 W
    Impedanssi: 4 Ohmia
    Tilaääni ja 3D-stereo - kaiuttimet muuttavat kaksikanavaisen stereoäänen mukaansatempaavaksi tilaääneksi.
    Liitännät: kaksi digitaalista optista sisääntuloa, yksi digitaalinen koaksiaalinen sisääntulo, kuusikanavainen suorasisääntulo, RCA-sisääntulo, 3.5mm-sisääntuloYhteensopivat laitteet: tietokoneet, musiikkisoittimet, televisiot, Blu-Ray/DVD-soittimet, pelikonsolit sekä muut äänilähteet
    Lisäominaisuudet: THX-sertifioidut kaiuttimet, Dolby Digital 5.1 -dekoodaus, DTS-dekoodaus, 3D Stereo -toiminto (tilaäänet kaksikanavaisista lähteistä), ohjausyksikkö (pinottava), neljä satelliittikaiutinta (seinäkiinnitettävät), keskikaiutin (seinäkiinnitettävä), subwoofer sivuelementillä, kaukosäädin
    Pakkauksen sisältö: kaiuttimet, kaiutinjohdot, kuusikanavaisen suorasisääntulon kaapeli (1.8m), pinottava ohjausyksikkö, kaukosäädin, kolme AAA-paristoa, käyttöopas, kahden vuoden valmistajan takuu ja täysi tuotetuki", 250, 2);


INSERT INTO tuote (id, nimi, kuvaus, hinta, tuoteryhmanro)
VALUES (24, "Audio-Technica AT-LP60XBT-BK -levysoitin, musta", "Klassinen vinyylisoundi ilman kaapeleita. Täysin automaattinen, hihnavetoinen AT-LP60XBT-stereolevysoitin Bluetooth-yhteydellä on mainio valinta, olipa kiinnostus vinyylilevyihin vasta heräillyt tai vanha levykokelma ja inspiraatio putsattu pölyistä. Levysoitin on yhdistettävissä kaiuttimiin, kuulokkeisiin ja langattomiin laitteisiin ja CSR BT -sirun myötä myös Bluetooth atpX -kodeekkia tukeviin laitteisiin. Voit asentaa soittimen eri huoneeseen kuin vahvistimen ja nauttia älppäreistä ilman kaapelisotkuja. Sisäänrakennetulla, kytkettävällä phono-esivahvistimella varustettu hihnavetoinen levysoitin soittaa 33 1/3 tai 45 RPM nopeudella ja sisältää uudistetun äänivarren ja kelkan. Laadun takaa Audio-Technican 50 vuoden kokemus äänirasioissa eikä AT-LP60XBT -mallin Dual Magnet -äänirasia ei tee poikkeusta. Levysoitin sisältää kaikki klassiset ominaisuudet, kahden eri levynopeuden toistomahdollisuuden, RCA-ulostuloliitännän ja irroitettavan pölykannen.

Ominaisuudet:

    Hihnavetoinen
    Täysautomaatti
    Moottori: DC servo-ohjattu
    Nopeudet: 33 1/3, 45 RPM
    Kytkettävä phono-esivahvistin
    Alusta: Alumiini
    Signal-to-Noise Ratio: >50 dB (DIN-B)
    Bluetooth 5.0
    Bluetooth aptX -yhteensopiva
    Mitat: 359,5 x 97,5 x 373,3 mm
    Mukana tulevat tarvikkeet: 3,5 - RCA -kaapeli, 45 RPM adapteri ja pölykansi", 189, 4);


INSERT INTO tuote (id, nimi, kuvaus, hinta, tuoteryhmanro)
VALUES (25, "Sandisk Clip Sport Plus -MP3-soitin, 16 Gb, musta", "Sandisk Clip Sport Plus on kevyt MP3-soitin, joka kulkee mukana missä tahansa oletkin. Vankka sateenkestävä rakenne mahdollistaa musiikin kuuntelun monissa eri ympäristöissä. Käytön joustavuutta lisää entisestään Bluetooth-yhteys, jonka ansiosta voit sujauttaa soittimen turvalliseen paikkaan ja nauttia musiikista langattomasti. Runsaan 16 gigatavun muistilla tallennat laitteeseen suuren määrän musiikkia, josta voit valita tilanteeseen sopivimmat kappaleet. Bluetooth-yhteyden lisäksi MP3-soittimessa on FM-radio.

Ominaisuudet:

    Tallenustila: 16 Gb
    Bluetooth
    Tuetut audiomuodot: MP3, WMA, AAC, WAV, FLAC, Audible
    Näyttö: 1,44 TFT LCD
    FM-radioviritin
    Akunkesto: jopa 20 tuntia
    Liitäntä: Micro-USB 2.0", 64, 4);

INSERT INTO tuote (id, nimi, kuvaus, hinta, tuoteryhmanro)
VALUES (26, "Sony CFD-S70 BoomBox -CD-radio, musta", "CFD-S70 Boombox -soittimessa yhdistyvät vaikuttava ääni ja kompakti koko. Tässä näppärässä yksikössä on AM/FM-radio sekä MP3-toistoa tukeva CD-soitin ja kasettisoitin. Sonyn Mega Bass -teknologia takaa täyteläisen äänen. Voit esiasettaa jopa 30 radioasemaa ja lisätä 3 asemaa One touch -suosikkeihin. Voit määrittää uniajastimen, joka sammuttaa radion automaattisesti tietyn ajan kuluttua.

Ominaisuudet:

    CD-soitin, tukee MP3-levyjä
    AM/FM-radioviritin
    Mega Bass -äänitehoste
    Uniajastin
    3,5 mm -liitäntä
    Virtalähde: AC-adapteri tai 6 x C-paristo
    Mitat: 228 x 351 x 158 mm
    Paino: 1,9 kg", 64, 4);

