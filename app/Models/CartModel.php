<?php namespace App\Models;

use CodeIgniter\Model;

class CartModel extends Model {

    protected $table = 'tuote';

    

    public function getProducts() {
        $this->table('tuote');
        $this->select('id,nimi,hinta');
        $query = $this->get();
        return $query->getResultArray();
    }

    public function clearCart() {
        $builder= $this->table('ostoskori');
        $builder->emptyTable('ostoskori');

    }

    public function deleteFromCart($id){

        $db = \Config\Database::connect();
        $db->query('DELETE FROM ostoskori WHERE id='.$id.'');

    }
}