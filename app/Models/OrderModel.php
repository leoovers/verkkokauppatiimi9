<?php namespace App\Models;
use CodeIgniter\Model;

use App\Models\CustomerModel;
use App\Models\OrderRowModel;

class OrderModel extends Model {
    protected $table= 'tilaus';

    protected $allowedFields = ['tila','asiakas_id'];

    public function saveAll($customer, $product_ids){
        $this->db->transStart();

        $customer_id = $this->saveCustomer($customer);
       
        $order_id = $this->saveOrder($customer_id);

        $this->saveOrderRows($order_id,$product_ids);
        $this->db->transComplete();

    }


    private function saveCustomer($customer) {

        $customerModel = new CustomerModel();
        $customerModel->save($customer);
        return $this->insertID();

    }

    private function saveOrder($customer_id) {

        $this->save([
            'tila' =>'tilattu',
            'asiakas_id' => $customer_id
        ]);
        return $this->insertID();
    }

    private function saveOrderRows($order_id,$product_ids) {

        $orderRowModel = new OrderRowModel();
        foreach ($product_ids as $product_id) {
            $orderRowModel->save([
                'tilaus_id' => $order_id,
                'tuote_id' => $product_id,
                'maara' => 1
            ]);
        }     

    }

}