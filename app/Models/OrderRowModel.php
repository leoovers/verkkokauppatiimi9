<?php  namespace App\Models;

use CodeIgniter\Model;

class OrderRowModel extends Model {
  protected $table = 'tilausrivi';

  protected $allowedFields = ['tilaus_id','tuote_id','maara'];
}