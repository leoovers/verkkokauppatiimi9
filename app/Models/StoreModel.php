<?php namespace App\Models;

use CodeIgniter\Model;

class StoreModel extends Model {
    protected $table = 'tuote';
    protected $allowedFields = ['id','nimi','kuvaus','hinta'];

    public function Getforarray($idt) {
        $return = array(
           
            
        );
        
        foreach ($idt as $id) {
          $this->table('tuote');
          $this->select('id,nimi,hinta');
          $this->where('id',$id);
          $query = $this->get();
          
          $product = $query->getRowArray();
          $this->AddToArray($product,$return);
            
          $this->resetQuery();
        }
       
        return $return;
    }
    private function AddToArray($product,&$array) {
        for ($i = 0;$i < count($array);$i++ )
        {
          if ($array[$i]['id'] === $product['id'])
          {
            $array[$i]['maara'] = $array[$i]['maara']  + 1;
            return;
          }
        }
        $product['maara'] = 1;
        array_push($array,$product);
      }
    
    public function getProducts() {
        $this->table('tuote');
        $this->select('id,nimi,kuvaus,hinta');
        $query = $this->get();
        return $query->getResultArray();
    }
    public function getHeadphones() {
        
        $this->table('tuote');
        $this->select('id,nimi,kuvaus,hinta');
        $this->where('tuoteryhmanro',1);
        $query = $this->get();
        return $query->getResultArray();
    }

    public function getSpeakers() {
        
        $this->table('tuote');
        $this->select('id,nimi,kuvaus,hinta');
        $this->where('tuoteryhmanro',2);
        $query = $this->get();
        return $query->getResultArray();
    }

    public function getMicrophones() {
        
        $this->table('tuote');
        $this->select('id,nimi,kuvaus,hinta');
        $this->where('tuoteryhmanro',3);
        $query = $this->get();
        return $query->getResultArray();
    }
    public function getPlayers() {
        
      $this->table('tuote');
      $this->select('id,nimi,kuvaus,hinta');
      $this->where('tuoteryhmanro',4);
      $query = $this->get();
      return $query->getResultArray();
  }

    public function getProduct($id) {
        
        $this->table('tuote');
        $this->select('id,nimi,kuvaus,hinta');
        $this->where('id',$id);
        $query = $this->get();
        return $query->getResultArray();
    }

    
    




}