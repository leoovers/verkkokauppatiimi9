<div class="row ">
	<div class="col-lg-12">
		<div class="panel panel-info">
			<div class="panel-heading">
				<div class="panel-title">
					<div class="row">
						<div class="col-xs-6">
							<h3><span class="glyphicon glyphicon-shopping-cart"></span> Ostoskori</h3>
						</div>

					</div>
				</div>
			</div>
			<?php $summa = 0 ?>
			<div class="panel-body">
				<?php foreach ($cart as $entities) : ?>
					<div class="row" style="margin-top:50px;">
						<div class="col-sm-2">
							<div class="thumb-wrapper">
								<div class="img-box">
									<img src="/images/products/<?= $entities["id"] ?>.png" style="max-height: 100px;" class="img-responsive img-fluid" alt="">
								</div>

							</div>
						</div>
						<div class="col-md-6">
							<h5 class="product-name"><strong><?= $entities["nimi"] ?></strong></h5>
						</div>
						<div class="col-md-1">
							<strong>X<?= $entities["maara"] ?> </strong>
						</div>
						<div class="col-md-1">
							<a class="" href="<?= site_url('Cart/deleteFromCart/' . $entities['id']) ?>">
							Poista
							</a>

						</div>
						<div class="col-md-2 text-right float-right mr-auto">
							<h5><strong><?= $entities["hinta"] ?>€ </strong></h5>
						</div>



					</div>
					<hr>
					<?php $summa += $entities["hinta"] * $entities["maara"]; ?>
				<?php endforeach; ?>
			</div>
			<div class="panel-footer">
				<div class="row text-center">



					<div class="col-md-3">
						<a type="button" href="/store/Order" class="btn btn-success btn-block">
							Tilaa
						</a>
					</div>
					<div class="col-md-6"></div>
					<div class="col-xs-9">
						<h4 class="text-right">Yhteensä <strong><?= $summa ?>€</strong></h4>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>