<h2 style="margin:30px;">Kuulokkeet</h2>
<div class="row-fluid">

<div class="card-deck"> 

<?php foreach ($headphones as $headphone) : ?>
<div class="card-columns-fluid">
<div class="col-sm-4">
  <div class="card card-margin" style="margin:30px; height:20rem; width: 18rem;">
    <div class="text-center" style="height:150px; overflow:hidden; padding-top:20px;">
      <a href="<?php echo base_url()?>/store/product/<?= $headphone['id']?>">
        <img class="img-responsive" style="object-fit: cover; height: 100%;" src="/images/products/<?= $headphone["id"]?>.png" alt="Card image cap">
      </a> 
    </div>
    <div class="card-body d-flex flex-column">
      <h5 class="card-title"><?= $headphone['nimi'] ?></h5>
      <p class="card-text" style="font-size: 1.2rem;"><?= $headphone['hinta'] ?>€</p>
      <a href="/store/product/<?=$headphone["id"]?>"  class="btn btn-primary mt-auto">Tuotekuvaus</a>
    </div>
  </div>
  </div>
</div>

<?php endforeach; ?>
</div>
</div>