
<div class="row-lg">
    <div class="col-lg-12" style="margin-bottom:120px;">
    <p style="font-size: 1.2rem;">Noise Outlet on meidän käsityksemme ihanteellisesta audiokaupasta. Meiltä löydät asiantuntevaa palvelua sekä kilpailukykyiset hinnat. Jatkuvasti kasvavaan tuotevalikoimaamme sisältyy kuulokkeita, kaiuttimia ja mikrofoneja.
		Voit selata tuotteita kategorioittain tai valita alla olevasta näyteikkunasta. </p>

	
    </div>
	
    <div class="col-md-12 carousel-color">
			<h2><b>Viikon suositukset</b></h2>
			<div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="0">
			<!-- Carousel indicators -->
			<ol class="carousel-indicators">
				<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
				<li data-target="#myCarousel" data-slide-to="1"></li>
				<li data-target="#myCarousel" data-slide-to="2"></li>
			</ol>
			   
			<!-- Wrapper for carousel items -->
			<div class="carousel-inner">
				<div class="item carousel-item active">
					<div class="row">
					<?php $i = 0; foreach ($products as $key => $product) :?>
						<div class="col-sm-3">
							<div class="thumb-wrapper">
								<div class="img-box">
									<a href="<?php echo base_url()?>/store/product/<?= $product['id']?>">
										<img src="/images/products/<?= $product["id"]?>.png" class="img-responsive img-fluid" alt="">
									</a>
								</div>
								<div class="thumb-content">
									<h4><?=$product["nimi"]?></h4>
									<p class="item-price"> <b> <span><?=$product["hinta"]?>€</span></b></p>
									
									<a href="<?php echo base_url()?>/store/product/<?= $product['id']?>" class="btn btn-primary">Lue lisää</a>
								</div>						
							</div>
						</div>
						<?php if($product == $product){unset($products[$key]);} if(++$i >= 4)break;  endforeach?>
						
					</div>
				</div>
				<div class="item carousel-item">
				<div class="row">
					<?php $i = 0; foreach ($products as $key => $product) :?>
						<div class="col-sm-3">
							<div class="thumb-wrapper">
								<div class="img-box">
									<a href="<?php echo base_url()?>/store/product/<?= $product['id']?>">
										<img src="/images/products/<?= $product["id"]?>.png" class="img-responsive img-fluid" alt="">
									</a>
								</div>
								<div class="thumb-content">
									<h4><?=$product["nimi"]?></h4>
									<p class="item-price"> <b> <span><?=$product["hinta"]?>€</span></b></p>
									
									<a href="<?php echo base_url()?>/store/product/<?= $product['id']?>" class="btn btn-primary" class="btn btn-primary">Lue lisää</a>
								</div>						
							</div>
						</div>
						<?php if($product == $product){unset($products[$key]);} if(++$i >= 4)break;  endforeach?>
						
					</div>
				</div>
				<div class="item carousel-item">
				<div class="row">
					<?php $i = 0; foreach ($products as $key => $product) :?>
						<div class="col-sm-3">
							<div class="thumb-wrapper">
								<div class="img-box">
									<a href="<?php echo base_url()?>/store/product/<?= $product['id']?>">
										<img src="/images/products/<?= $product["id"]?>.png" class="img-responsive img-fluid" alt="">
									</a>
								</div>
								<div class="thumb-content">
									<h4><?=$product["nimi"]?></h4>
									<p class="item-price"> <b> <span><?=$product["hinta"]?>€</span></b></p>
									
									<a href="<?php echo base_url()?>/store/product/<?= $product['id']?>" class="btn btn-primary" class="btn btn-primary">Lue lisää</a>
								</div>						
							</div>
						</div>
						<?php if($product == $product){unset($products[$key]);} if(++$i >= 4)break;  endforeach?>
						
					</div>
				</div>
			</div>
			<!-- Carousel controls -->
			<a class="carousel-control left carousel-control-prev" href="#myCarousel" data-slide="prev">
				<i class="fa fa-angle-left"></i>
			</a>
			<a class="carousel-control right carousel-control-next" href="#myCarousel" data-slide="next">
				<i class="fa fa-angle-right"></i>
			</a>
		</div>
		</div>
</div>