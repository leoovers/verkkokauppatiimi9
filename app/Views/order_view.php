<form method="post" action="<?= site_url('order/submit/');?>">
  <div class="form-row ">
    <div class="col-md-4 mb-3">
      <label for="validationCustom01">Etunimi</label>
      <input type="text" class="form-control" name="fname" placeholder="Etunimi" value="" required>
      <div class="valid-feedback">
        Looks good!
      </div>
    </div>
    <div class="col-md-4 mb-3">
      <label for="validationCustom02">Sukunimi</label>
      <input type="text" class="form-control" name="lname" placeholder="Sukunimi" value="" required>
      <div class="valid-feedback">
        Looks good!
      </div>
    </div>
    <div class="col-md-4 mb-3">
      <label for="validationCustom02">Sähköposti</label>
      <input type="text" class="form-control" name="email" placeholder="Sähköposti" value="" required>
      <div class="valid-feedback">
        Looks good!
      </div>
    </div>
  </div>
  <div class="form-row">
    <div class="col-md-6 mb-3">
      <label for="validationCustom03">Postitoimipaikka</label>
      <input type="text" class="form-control" name="city" placeholder="Postitoimipaikka" required>
      <div class="invalid-feedback">
        Please provide a valid city.
      </div>
    </div>
    <div class="col-md-3 mb-3">
      <label for="validationCustom04">Katuosoite</label>
      <input type="text" class="form-control" name="address" placeholder="Katuosoite" required>
      <div class="invalid-feedback">
        Please provide a valid state.
      </div>
    </div>
    <div class="col-md-3 mb-3">
      <label for="validationCustom05">Postinumero</label>
      <input type="text" class="form-control" name="zip" placeholder="Postinumero" required>
      <div class="invalid-feedback">
        Please provide a valid zip.
      </div>
    </div>
  </div>
 
  <button class="btn btn-primary" type="submit">Tilaa</button>
</form>

