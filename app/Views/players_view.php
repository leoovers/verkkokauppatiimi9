<h2 style="margin:30px;">Soittimet</h2>
<div class="row-fluid">
<div class="card-deck"> 
<?php foreach ($players as $player) : ?>
  <div class="card-columns-fluid">
    <div class="col-sm-4">
  <div class="card card-margin" style="margin:30px; height:25rem; width: 18rem;">
    <div class="text-center" style="height:225px; overflow:hidden; padding-top:20px;">
      <a href="<?php echo base_url()?>/store/product/<?= $player['id']?>">
        <img class="img-responsive" style="object-fit: cover; height: 65%; margin-top:20px; " src="/images/products/<?= $player["id"]?>.png" alt="Card image cap">
      </a>  
    </div>
    <div class="card-body d-flex flex-column">
      <h5 class="card-title"><?= $player['nimi'] ?></h5>
      <p class="card-text" style="font-size: 1.2rem;"><?= $player['hinta'] ?>€</p>
      <a href="/store/product/<?=$player["id"]?>" class="btn btn-primary mt-auto">Tuotekuvaus</a>
    </div>
  </div>
    </div>
  </div>
<?php endforeach; ?>
</div>
</div>