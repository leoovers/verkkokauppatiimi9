    <?php foreach ($product as $item) : ?>

    	<div class="container">
    		<div class="content-wrapper">
    			<div class="item-container">
    				<div class="container">
    					<div class="col-md-12">
    						<div class="product col-md-4">
    							<center>
    								<a href="<?php echo base_url()?>/store/product/<?= $item['id']?>">
										<img src="/images/products/<?= $item["id"] ?>.png" style="max-height: 300px; margin-bottom:50px;" class="img-responsive img-fluid" alt="">
									</a>
    							</center>
    						</div>

    					</div>
    					<div class="row">
    						<div class="col-md-6">
    							<h5><?= $item['nimi'] ?></h5>
    						</div>
    						<div class="col-md-2">
    							<h6 class="item-price"><?= $item['hinta'] ?>€</h6>
    						</div>
    						<div class="thumb-content">
    							<a type="button" href="<?php echo base_url() ?>/store/add/<?= $item['id'] ?>" class="button btn btn-dark">Lisää koriin</a>
    						</div>

    					</div>
    				</div>
    			</div>
    			<div class="container-fluid">
    				<div class="col-md-12 product-info">
    					<ul id="myTab" class="nav nav-tabs">
    						<li class="active"><a href="#service-one" data-toggle="tab">LISÄTIEDOT</a></li>
    					</ul>
    					<div id="myTabContent" class="tab-content">
    						<div class="tab-pane fade in active" id="service-one">
    							<p><?= $item["kuvaus"] ?></p>
    						</div>
    					</div>
    					<hr>
    				</div>
    			</div>

    		</div>
    	</div>
    <?php endforeach; ?>