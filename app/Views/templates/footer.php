</div>

<!-- Footer -->
<footer class="navbar footer text-white fixed-bottom  footer-color footer-shadow content container-fluid">

  <!-- Footer Links -->
  <div class="container-fluid text-center text-md-left">

    <!-- Grid row -->
    <div class="row">

      <!-- Grid column -->
      <div class="col-md-6 mt-md-0 mt-3">

        <!-- Content -->
        <h5 class="text-uppercase">Yhteystiedot</h5>
        <p>Puhelinnumero: <br> 0400112112
        <p>Sähköposti: <br> NoiseOutlet@hotmail.com</p>

      </div>
      <!-- Grid column -->

      <hr class="clearfix w-100 d-md-none pb-3">

      <!-- Grid column -->
      <div class="col-md-3 mb-md-0 mb-3">

        <!-- Links -->
        <h5 class="text-uppercase">Seuraa meitä</h5>

        <ul class="list-unstyled">
          <li>
            <a href="https://www.instagram.com/officialrickastley/?hl=fi">Instagram</a>
          </li>
          <li>
            <a href="https://twitter.com/rickastley">Twitter</a>
          </li>

        </ul>

      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-3 mb-md-0 mb-3">

        <!-- Links -->
        <h5 class="text-uppercase">Lisää meistä</h5>

        <ul class="list-unstyled">
          <li>
            <a href="https://www.youtube.com/watch?v=dQw4w9WgXcQ">Youtube</a>
          </li>
          <li>
            <a href="https://fi.wikipedia.org/wiki/Rick_Astley">Wikipedia</a>
          </li>

        </ul>

      </div>
      
      <!-- Grid column -->

    </div>
    <img class="img-responsive" style="max-width:20%;" src="\noiseoutlet.png" alt="">

  </div>
  <!-- Footer Links -->

  <!-- Copyright -->

  <!-- Copyright -->

</footer>
<!-- jQuery, popper, bootstrap js -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>