<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/css/carousel.css">

    <!-- Font awesome icons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Noise Outlet</title>
</head>
<body class="body-color">
    <!-- Navigation bar -->
 
<nav class="navbar navbar-color navbar-expand-md justify-content-center">
    <a href="/store" class=" navbar-brand d-flex w-50 mr-auto"> <img class="img-responsive" style="max-width:20%;" src="\noiseoutlet.png" alt=""></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsingNavbar3">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="navbar-collapse collapse w-100" id="collapsingNavbar3">
        <ul class="navbar-nav w-100 justify-content-center">

            <li class="nav-item">
                <a class="nav-link text-white" href="/store/headphones">Kuulokkeet</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white" href="/store/speakers">Kaiuttimet</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white" href="/store/microphones">Mikrofonit</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white" href="/store/Players">Soittimet</a>
            </li>
        </ul>
        <ul class="nav navbar-nav ml-auto w-100 justify-content-end">
            <li class="nav-item">
            <li><a href="/store/Cart"><span class="fa fa-shopping-cart"></span> Ostoskori</a></li>
            </li>

        </ul>
    </div>
</nav>
<div class="container">

